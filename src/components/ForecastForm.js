import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { fetchForecast } from '../actions/forecast';
import loading from '../loading.gif';


class ForecastForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            'city': 'Lyon',
        }
    }

    changeCity = (event) => {
        this.setState({
            'city': event.target.value
        })
    }

    fetchForecast = () => {
        this.props.fetchForecast(this.state.city);
    }

    render() {
        return (
            <div>
                <input onChange={this.changeCity} value={this.state.city} type="text" />
                {
                    this.props.loader === false ?
                        <button onClick={this.fetchForecast}>Rechercher</button>
                        :
                        <div>
                            <img src={loading} className="App-loading" alt="loading" />
                    LOADING</div>
                }
            </div>
        );
    }
}

const mapStateToProps = (state /*, ownProps */) => {
    return {
        loader: state.forecastReducer.loader
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        fetchForecast,
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ForecastForm)
